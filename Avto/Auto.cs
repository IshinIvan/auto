﻿using System;
namespace Avto
{
	public class Auto
	{
		public string mark;
		public int maxSpeed;

		public virtual double GetCost (int maxSpeed)
		{
			return (maxSpeed*100);
		}

		public virtual int Update (int maxSpeed)
		{
			return (maxSpeed + 10);
		}

		public virtual string Info (string mark, int maxSpeed)
		{
			return ("Марка автомобиля: "+ mark +
			        ", максимальная скорость: " + maxSpeed +
			        ", стоимость: " + GetCost(maxSpeed));
		}
	}
}
