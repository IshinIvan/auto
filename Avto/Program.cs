﻿using System;

namespace Avto
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Auto Auto = new Auto();
			Auto.mark = "BMW";
			Auto.maxSpeed = 140;

			BuisnessAuto BuisnessAuto = new BuisnessAuto();
			BuisnessAuto.mark = "Lexus";
			BuisnessAuto.maxSpeed = 160;

			Console.WriteLine(Auto.Info(Auto.mark,Auto.maxSpeed));
			Console.WriteLine(BuisnessAuto.Info(BuisnessAuto.mark,BuisnessAuto.maxSpeed));

			Auto.maxSpeed = Auto.Update(Auto.maxSpeed);
			BuisnessAuto.maxSpeed = BuisnessAuto.Update(BuisnessAuto.maxSpeed);

			Console.WriteLine(Auto.Info(Auto.mark,Auto.maxSpeed));
			Console.WriteLine(BuisnessAuto.Info(BuisnessAuto.mark,BuisnessAuto.maxSpeed));
		}
	}
}
